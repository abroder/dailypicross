#!/usr/bin/env node

var Canvas = require('canvas')
var fs = require('fs')
var Twit = require('twit')

const sizes = [{w: 10, h: 10}]

const createNonogram = function (w, h) {
  const result = {    
    output: new Canvas(600, 600),
    margins: { top: 140, left: 100, right: 10, bottom: 10 },
    w: w,
    h: h,
    puzzle: generatePuzzle(w, h)
  } 
  result.gridHeight = result.output.height -
    result.margins.top - result.margins.bottom
  result.gridWidth = result.output.width -
    result.margins.left - result.margins.right

  result.rowHeight = result.gridHeight / result.h
  result.rowWidth = result.gridWidth / result.w

  return result
}

const generatePuzzle = function(w, h) {
  const puzzle = []
  for (let i = 0; i < h; i++) {
    puzzle.push(new Array(h))
  }

  for (let x = 0; x < w; x++) {
    for (let y = 0; y < h; y++) {
      puzzle[y][x] = Math.random() > 0.5
    }
  }

  return puzzle
}

const makeGrid = function(nonogram) {
  const ctx = nonogram.output.getContext('2d')

  ctx.fillStyle = 'white'
  ctx.fillRect(0, 0, nonogram.output.width, nonogram.output.height)

  let y = nonogram.margins.top
  let x = nonogram.margins.left

  /* Draw rows */
  for (let i = 0; i < nonogram.h + 1; i++) {
    if ((i) % 5 === 0) {
      ctx.lineWidth = 3
    } else {
      ctx.lineWidth = 1
    }

    ctx.beginPath()
    ctx.moveTo(x, y)
    ctx.lineTo(x + nonogram.gridWidth, y)
    ctx.stroke()

    y = y + nonogram.rowHeight
  }

  /* Draw columns */
  y = nonogram.margins.top
  for (let i = 0; i < nonogram.h + 1; i++) {
    if ((i) % 5 === 0) {
      ctx.lineWidth = 3
    } else {
      ctx.lineWidth = 1
    }

    ctx.beginPath()
    ctx.moveTo(x, y)
    ctx.lineTo(x, y + nonogram.gridHeight)
    ctx.stroke()

    x += nonogram.rowWidth
  }
}

const makeRules = function (nonogram) {
  const ctx = nonogram.output.getContext('2d')
  ctx.fillStyle = 'black'
  ctx.font = "20px Times New Roman"
  ctx.textAlign = 'right'
  
  let baselineY = nonogram.margins.top + nonogram.rowHeight / 2 + 7

  for (let y = 0; y < nonogram.h; y++) {
    let rule = ""
    let cumulative = 0

    for (let x = 0; x < nonogram.w; x++) {
      if (nonogram.puzzle[y][x]) {
        cumulative += 1
      } else if (cumulative !== 0) {
        rule = rule + " " + cumulative
        cumulative = 0
      }
    }
    
    if (cumulative !== 0) {
      rule = rule + " " + cumulative
    }

    if (rule === "") rule = " 0"

    ctx.fillText(rule, nonogram.margins.left - 15, baselineY)
    baselineY += nonogram.rowHeight
  }

  let baselineX = nonogram.margins.left + nonogram.rowWidth / 2 + 7

  const metrics = ctx.measureText('1')
  const characterHeight = metrics.emHeightAscent + 
    metrics.emHeightDescent

  for (let x = 0; x < nonogram.w; x++) {
    let rule = []
    let cumulative = 0

    for (let y = 0; y < nonogram.h; y++) {
      if (nonogram.puzzle[y][x]) {
        cumulative += 1
      } else if (cumulative !== 0) {
        rule.push(cumulative)
        cumulative = 0
      }
    }

    if (cumulative !== 0) {
      rule.push(cumulative)
    }

    if (rule.length === 0) rule = [0]

    baselineY = nonogram.margins.top - 5
    for (let i = rule.length - 1; i >= 0; i--) {
      ctx.fillText(rule[i], baselineX, baselineY)
      baselineY -= characterHeight
    }
    baselineX += nonogram.rowWidth
  }
}

const makeWatermark = function (nonogram) {
  const d = new Date()
  const dateString = d.getFullYear() + '-' + 
	(d.getMonth() + 1) + '-' + d.getDate()

  const ctx = nonogram.output.getContext('2d')

  ctx.textAlign = 'left'
  ctx.fillText(dateString, 10, 25)
  ctx.fillText('@dailypicross', 10, 45)
}

const makeTweet = function (nonogram) {
  const T = new Twit(require(__dirname + "/config.js"))
  T.post('media/upload', { media_data: nonogram.output.toBuffer().toString('base64') }, (err, result) => {
    if (err) {
      return
    }

    T.post('statuses/update', {media_ids: [result.media_id_string]}, (err, result) => {
      if (err) {
        console.log(err)
        return
      }
    })
  })
}

const saveCanvas = function (canvas) {
  const out = fs.createWriteStream(__dirname + '/output.png')
  const stream = canvas.pngStream();

  stream.on('data', function(chunk){
      out.write(chunk);
  });

  stream.on('end', function() {});
}

const size = sizes[Math.floor(Math.random()*sizes.length)]

const nonogram = createNonogram(size.w, size.h)
makeGrid(nonogram)
makeRules(nonogram)
makeWatermark(nonogram)
makeTweet(nonogram)
